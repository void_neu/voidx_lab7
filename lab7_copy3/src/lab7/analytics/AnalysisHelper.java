/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab7.analytics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lab7.entities.Comment;
import lab7.entities.Post;
import lab7.entities.User;

/**
 *
 * @author Team Void
 */
public class AnalysisHelper {

    public void userWithMostLikes() {
        
        Map<Integer, Integer> userLikesCount = new HashMap<>();
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        
        for(User user : users.values()){
            
            for(Comment c : user.getComments()){
                
                int likes = 0;
                if(userLikesCount.containsKey(user.getId())){
                    
                    likes = userLikesCount.get(user.getId());
                }
                
                likes += c.getLikes();
                userLikesCount.put(user.getId(), likes);
            } 
        }
        
        int max = 0;
        int maxId = 0;
        
        for(int id : userLikesCount.keySet()){
            
            if(userLikesCount.get(id) > max){
                
                max = userLikesCount.get(id);
                maxId = id;
            }
        }
        
        System.out.println("User With Most Likes: "+max+"\n"+users.get(maxId));
        
        
    }
    // find user with Most Likes
    // TODO
    
    
    public void PostWithMostComments() {
        
       // Map<Integer, Posts> posts = new HashMap<>();
        Map<Integer,Post> posts = DataStore.getInstance().getPosts();
        List<Post> postList= new ArrayList<>(posts.values());
        Map<Integer,Comment> comments=DataStore.getInstance().getComments();
        List<Comment> commentList = new ArrayList<>(comments.values());
        int max=0;
        
        for (int i=0; i<postList.size();i++)
        {
            
            int sum=0;
            sum+=postList.get(i).getComments().size();
        
        
        if (sum>max){
                         max=sum;
        }
        
        
        }
        
        System.out.println("the post with max no of comments is "+max+"\n");
        
    }
        
//        for(User user : users.values()){
//            
//            for(Comment c : user.getComments()){
//                
//                int comments = 0;
//                if(userCommentCount.containsKey(user.getId())){
//                    
//                    comments = userCommentCount.get(user.getId());
//                }
//                
//                comments += c.getPostId();
//                userCommentCount.put(user.getId(), comments);
//            } 
//        }
        
//        int max = 0;
//        int maxId = 0;
//        
//        for(int id : userCommentCount.keySet()){
//            
//            if(userCommentCount.get(id) > max){
//                
//                max = userCommentCount.get(id);
//                maxId = id;
//            }
//        }
        
//        System.out.println("User With Most Comments: "+max+"\n"+users.get(maxId));
        
        
    
    
    
    // find 5 comments which have the most likes
    // TODO

   public void getFiveMostLikedComments() {
      
        
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        List<Comment> commentList = new ArrayList<>(comments.values());
        
        Collections.sort(commentList, new Comparator<Comment>(){
            
            
            @Override
            public int compare(Comment o1, Comment o2){
                
                return o2.getLikes() - o1.getLikes();
            }
        });
        
        System.out.println("Five Most Liked Comments: ");
        for(int i =0; i<commentList.size() && i<5; i++){
            
            System.out.println(commentList.get(i));
        }
        
        
    }
    
    
    
    
    
    
    
    
}
