/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab7.analytics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lab7.entities.Comment;
import lab7.entities.Post;
import lab7.entities.User;

/**
 *
 * @author Team Void
 */
public class AnalysisHelper {

    
    //**************************** Users With Most Likes ***************************
    
    public void userWithMostLikes() {

        Map<Integer, Integer> userLikesCount = new HashMap<>();
        Map<Integer, User> users = DataStore.getInstance().getUsers();

        for (User user : users.values()) {

            for (Comment c : user.getComments()) {

                int likes = 0;
                if (userLikesCount.containsKey(user.getId())) {

                    likes = userLikesCount.get(user.getId());
                }

                likes += c.getLikes();
                userLikesCount.put(user.getId(), likes);
            }
        }

        int max = 0;
        int maxId = 0;

        for (int id : userLikesCount.keySet()) {

            if (userLikesCount.get(id) > max) {

                max = userLikesCount.get(id);
                maxId = id;
            }
        }

        System.out.println("User With Most Likes: " + max + "\n" + users.get(maxId));

    }
    
    //********************** User with most comments *********************************

    public void userWithMostComments() {

        Map<Integer, Integer> userCommentCount = new HashMap<>();
        Map<Integer, User> users = DataStore.getInstance().getUsers();

        for (User user : users.values()) {

            for (Comment c : user.getComments()) {

                int comments = 0;
                if (userCommentCount.containsKey(user.getId())) {

                    comments = userCommentCount.get(user.getId());
                }

                comments += c.getPostId();
                userCommentCount.put(user.getId(), comments);
            }
        }

        int max = 0;
        int maxId = 0;

        for (int id : userCommentCount.keySet()) {

            if (userCommentCount.get(id) > max) {

                max = userCommentCount.get(id);
                maxId = id;
            }
        }

        System.out.println("User With Most Comments: " + max + "\n" + users.get(maxId));

    }

    //************************* Five Most Liked comments *******************************
    public void getFiveMostLikedComments() {

        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        List<Comment> commentList = new ArrayList<>(comments.values());

        Collections.sort(commentList, new Comparator<Comment>() {

            @Override
            public int compare(Comment o1, Comment o2) {

                return o2.getLikes() - o1.getLikes();
            }
        });

        System.out.println("Five Most Liked Comments: ");
        for (int i = 0; i < commentList.size() && i < 5; i++) {

            System.out.println(commentList.get(i));
        }
    }
        
      public void getTop5InactiveUsersBasedOnTotalCommentsTheyCreated() {

        Map<Integer, Comment> comments = DataStore.getInstance().getComments();

        Map<Integer, Integer> users = new HashMap<>();

        for (Comment c : comments.values()){

        c.getUserId();


        int count = 1;

        if(users.containsKey(c.getUserId())){
            count = users.get(c.getUserId());
            count++;
        }

        users.put(c.getUserId(),count);
        
        }
     
        Set entrySet = users.entrySet(); 
        List<Entry<Integer, Integer>> list = new ArrayList<>(entrySet);
        
        Collections.sort(list, new Comparator<Entry<Integer, Integer>>() {
            public int compare(Entry<Integer, Integer> a, Entry<Integer, Integer> b) {
                return a.getValue() - b.getValue();
            }
        });
        
        int prevNumberOfPosts = -1;
        int count = 0;
        for (int i = 0; i < list.size() && count <= 1; i++) {
            int userid = list.get(i).getKey();
            int numberOfPosts = list.get(i).getValue();
            
            if(prevNumberOfPosts != numberOfPosts){
                count++;
            }
            prevNumberOfPosts = numberOfPosts;
            System.out.println("User :" + userid + " posted " + numberOfPosts + " comments");
        }

        
    }
       

        
        
        public void getFiveInactiveBasedOnTotalPost() {
            
        Map<Integer, Post> p = DataStore.getInstance().getPosts();
        List<Post> inactiveUserList = new ArrayList<>(p.values());

        
        Collections.sort(inactiveUserList, new Comparator<Post>(){
            
        @Override
        public int compare(Post o1, Post o2) {

            return o2.getPostId() - o1.getPostId();
        } 
        });
        
        Collections.reverse(inactiveUserList);
        
        System.out.println("Five Most Inactive Users: ");
        
        for (int i = 0; i < inactiveUserList.size() && i < 5; i++){

            System.out.println(inactiveUserList.get(i));  
        }        
    }
    
    
        public List<Entry<Integer,Integer>> someName(final String order){
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();

        Map<Integer, Integer> totalCount = new HashMap<>();

        for (Comment c : comments.values()) {                
            totalCount.put(c.getUserId(), totalCount.getOrDefault(c.getUserId(), 0) + c.getLikes() + 1);   
        }

        for (Post p : posts.values()) {
            totalCount.put(p.getUserId(), totalCount.getOrDefault(p.getUserId(), 0) + 1);
        }
            
        Set<Map.Entry<Integer,Integer>> entrySet = totalCount.entrySet();
            
           
        List<Map.Entry<Integer, Integer>> list = new ArrayList<>(entrySet);
        Comparator<Entry<Integer, Integer>> cmp = new Comparator<Entry<Integer, Integer>>() {
            public int compare(Entry<Integer, Integer> a, Entry<Integer, Integer> b) {
                if(order.equals("asc")){
                    return a.getValue() - b.getValue();  
                }else{
                    return b.getValue() - a.getValue();
                }
                   
                   
            }
        };
           
        Collections.sort(list, cmp);
            
        return list;
    }    
        
        
    
    public void getTop5InactiveUsersOverallBasedonSumOfCommentsLikesAndPosts() {

            
            List<Map.Entry<Integer, Integer>> list = someName("asc");
            
            System.out.print("Top 5 Inactive user based on sum of likes,comments,posts are:");
            for (int i = 0; i < list.size() && i < 5; i++) {
            int userid = list.get(i).getKey();
            int totalSum = list.get(i).getValue();
            System.out.println("\n" + userid + " : " + totalSum );
        }               
    }
    
    

}
